import asyncio
async def counter(id, time):
    for i in range(time+1):
        if i == 0:
            print(f'Counter {id}: starting...')
        elif i == time:
            print(f'Counter {id}: {i}')
            print(f'Counter {id}: finishing...')
        else:
            print(f'Counter {id}: {i}')
        await asyncio.sleep(1)


async def main():
    await asyncio.gather(counter('A', 4), counter('B', 2), counter('C', 6))

asyncio.run(main())
